﻿using System.Drawing;
using System.Linq;
using Point = TanksWinForms.Model.Basic.Point;

namespace TanksWinForms.Helpers
{
    public static class PointsHelper
    {
        public static PointF[] ToPointsF(this Point[] points)
        {
            return points.Select(point => new PointF((int)point.X, (int)point.Y)).ToArray();
        }
    }
}
