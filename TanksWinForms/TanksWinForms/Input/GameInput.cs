﻿using System;
using System.Threading;
using System.Threading.Tasks;
using SharpDX.DirectInput;

namespace TanksWinForms.Input
{
    public static class GameInput
    {
        // события клавиатуры

        public static event Action OnUpPressed;
        public static event Action OnDownPressed;
        public static event Action OnLeftPressed;
        public static event Action OnRightPressed;

        public static void StartListingKeyboard()
        {
            var task = new Task(() =>
            {
                MainForKeyboard();
            });
            task.Start();
        }

        private static void MainForKeyboard()
        {
            // Initialize DirectInput
            var directInput = new DirectInput();

            // Instantiate the joystick
            var keyboard = new Keyboard(directInput);

            // Acquire the joystick
            keyboard.Properties.BufferSize = 128;
            keyboard.Acquire();

            // Poll events from joystick
            while (true)
            {
                keyboard.Poll();

                if (keyboard.GetCurrentState().IsPressed(Key.Left))
                    OnLeftPressed?.Invoke();

                if (keyboard.GetCurrentState().IsPressed(Key.Right))
                    OnRightPressed?.Invoke();

                if (keyboard.GetCurrentState().IsPressed(Key.Up))
                    OnUpPressed?.Invoke();

                if (keyboard.GetCurrentState().IsPressed(Key.Down))
                    OnDownPressed?.Invoke();
                
                Thread.Sleep(30);
            }
        }
    }

}
