﻿using System.Collections.Generic;
using TanksWinForms.Input;
using TanksWinForms.Model.Basic;

namespace TanksWinForms.Model
{
    public class Game
    {
        public Game()
        {
            GameInput.StartListingKeyboard();

            GameInput.OnUpPressed += () => { PlayerControl(ControlEnum.Move); };
            GameInput.OnLeftPressed += () => { PlayerControl(ControlEnum.TurnLeft); };
            GameInput.OnRightPressed += () => { PlayerControl(ControlEnum.TrunRight); };

            GameInput.OnDownPressed += () => { /* do nothing */ };

            this.PlayerGameObject = new Tank();

            this.GameObjects = new List<GameObject>()
            {
                this.PlayerGameObject
            };
        }

        public GameObject PlayerGameObject;

        public IList<GameObject> GameObjects;

        public void PlayerControl(ControlEnum controlEnum)
        {
            switch (controlEnum)
            {
                case ControlEnum.TurnLeft:
                    PlayerGameObject.Turn(GameObject.TurnEnum.Left);
                    break;
                case ControlEnum.TrunRight:
                    PlayerGameObject.Turn(GameObject.TurnEnum.Right);
                    break;
                case ControlEnum.Move:
                    PlayerGameObject.Move();
                    break;
                case ControlEnum.Down:
                    // do nothing
                    break;
                case ControlEnum.ZoomIn:
                    PlayerGameObject.ReScale(GameObject.ReScaleEnum.Increase);
                    break;
                case ControlEnum.ZoomOut:
                    PlayerGameObject.ReScale(GameObject.ReScaleEnum.Decrease);
                    break;
            }
        }
    }
}
