﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using TanksWinForms.Model.Basic;
using Point = TanksWinForms.Model.Basic.Point;

namespace TanksWinForms.Model
{
    internal class Tank : GameObject
    {
        public Tank()
        {
            _alpha = 0;
        }

        public override Point[] GetDrawPoints()
        {
            var arrayPoints = new List<int[]>
            {
                new[] {-100, -100},
                new[] {100, -100},
                new[] {100, 100},
                new[] {-100, 100},
                new[] {-100, -100},

                // to Continue here
                new[] {-150, -200},
                
            };

            return arrayPoints
                .Select(x => GetPoint(x[0], x[1]))
                .ToArray();
        }
    }
}
