﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanksWinForms.Model
{
    /// <summary>
    /// Управление игрой
    /// </summary>
    public enum ControlEnum
    {
        TurnLeft,
        TrunRight,
        Move,
        Down,
        ZoomIn,
        ZoomOut
    }
}
