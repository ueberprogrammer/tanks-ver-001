﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace TanksWinForms.Model.Basic
{
    public abstract class GameObject
    {
        private int speed = 5;

        private double _scale = 1;

        protected double _alpha = 30; // подсказка: 30 градусов в радианах =  30 / (180 / PI)

        private Point _position = new Point(400, 400);

        private double _radionConst = 180 / Math.PI;

        public abstract Point[] GetDrawPoints();

        protected Point GetPoint(double x, double y)
        {
            var alpha1 = this._alpha;

            if (x < 0 && y < 0)
            {
                alpha1 += 180;
            }
            else if (x < 0)
            {
                alpha1 += 90;
            }
            else if (y < 0)
            {
                alpha1 += 270;
            }

            var sin = Math.Sin(alpha1 / _radionConst);
            var cos = Math.Cos(alpha1 / _radionConst);
            var length = Math.Sqrt(x * x + y * y);

            return new Point(cos * Math.Abs(length * _scale) + _position.X,
                sin * Math.Abs(length * _scale) + _position.Y);
        }

        public void ReScale(ReScaleEnum reScaleEnum)
        {
            switch (reScaleEnum)
            {
                case ReScaleEnum.Increase:
                    _scale += 0.1;
                    break;
                case ReScaleEnum.Decrease:
                    _scale -= 0.1;
                    break;
            }
        }

        public void Turn(TurnEnum turnEnum)
        {
            switch (turnEnum)
            {
                case TurnEnum.Right:
                    _alpha -= 10;
                    break;
                case TurnEnum.Left:
                    _alpha += 10;
                    break;
            }
        }

        public enum ReScaleEnum
        {
            Increase,
            Decrease
        }

        public enum TurnEnum
        {
            Left,
            Right
        }

        public void Move()
        {
            _position.X += Math.Sin(_alpha / _radionConst) * speed;
            _position.Y += Math.Cos(_alpha / _radionConst) * speed;
        }
    }
}
