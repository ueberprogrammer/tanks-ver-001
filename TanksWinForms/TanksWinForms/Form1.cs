﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TanksWinForms.Helpers;
using TanksWinForms.Input;
using TanksWinForms.Model;

namespace TanksWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            globalGameTimer.Start();
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Старт!");
        }

        private static Game game = new Game();

        private void gamePanel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawLines(new Pen(Brushes.Black), 
                game.PlayerGameObject.GetDrawPoints()
                    .ToPointsF());
        }

        private void gameGlobalTimer_Tick(object sender, EventArgs e)
        {
            gamePanel.Refresh();
        }

        private void btn_SizeDecrese_Click(object sender, EventArgs e)
        {
            game.PlayerControl(ControlEnum.ZoomIn);
        }

        private void btn_SizeIncrease_Click(object sender, EventArgs e)
        {
            game.PlayerControl(ControlEnum.ZoomOut);
        }

        private void btn_Right_Click(object sender, EventArgs e)
        {
            game.PlayerControl(ControlEnum.TrunRight);
        }

        private void btn_Left_Click(object sender, EventArgs e)
        {
            game.PlayerControl(ControlEnum.TurnLeft);
        }

        private void btn_Foward_Click(object sender, EventArgs e)
        {
            game.PlayerControl(ControlEnum.Move);
        }
    }
}
