﻿using TanksWinForms.CustomControls;

namespace TanksWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btn_Start = new System.Windows.Forms.Button();
            this.globalGameTimer = new System.Windows.Forms.Timer(this.components);
            this.btn_SizeDecrese = new System.Windows.Forms.Button();
            this.btn_SizeIncrease = new System.Windows.Forms.Button();
            this.btn_Left = new System.Windows.Forms.Button();
            this.btn_Right = new System.Windows.Forms.Button();
            this.gamePanel = new TanksWinForms.CustomControls.DoubleBufferedPanel();
            this.btn_Foward = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_Start
            // 
            this.btn_Start.Location = new System.Drawing.Point(12, 735);
            this.btn_Start.Name = "btn_Start";
            this.btn_Start.Size = new System.Drawing.Size(124, 23);
            this.btn_Start.TabIndex = 1;
            this.btn_Start.Text = "Start";
            this.btn_Start.UseVisualStyleBackColor = true;
            this.btn_Start.Click += new System.EventHandler(this.btn_Start_Click);
            // 
            // globalGameTimer
            // 
            this.globalGameTimer.Interval = 10;
            this.globalGameTimer.Tick += new System.EventHandler(this.gameGlobalTimer_Tick);
            // 
            // btn_SizeDecrese
            // 
            this.btn_SizeDecrese.Location = new System.Drawing.Point(932, 734);
            this.btn_SizeDecrese.Name = "btn_SizeDecrese";
            this.btn_SizeDecrese.Size = new System.Drawing.Size(124, 23);
            this.btn_SizeDecrese.TabIndex = 2;
            this.btn_SizeDecrese.Text = "Меньше";
            this.btn_SizeDecrese.UseVisualStyleBackColor = true;
            this.btn_SizeDecrese.Click += new System.EventHandler(this.btn_SizeDecrese_Click);
            // 
            // btn_SizeIncrease
            // 
            this.btn_SizeIncrease.Location = new System.Drawing.Point(1075, 734);
            this.btn_SizeIncrease.Name = "btn_SizeIncrease";
            this.btn_SizeIncrease.Size = new System.Drawing.Size(124, 23);
            this.btn_SizeIncrease.TabIndex = 2;
            this.btn_SizeIncrease.Text = "Больше";
            this.btn_SizeIncrease.UseVisualStyleBackColor = true;
            this.btn_SizeIncrease.Click += new System.EventHandler(this.btn_SizeIncrease_Click);
            // 
            // btn_Left
            // 
            this.btn_Left.Location = new System.Drawing.Point(519, 734);
            this.btn_Left.Name = "btn_Left";
            this.btn_Left.Size = new System.Drawing.Size(124, 23);
            this.btn_Left.TabIndex = 3;
            this.btn_Left.Text = "Лево";
            this.btn_Left.UseVisualStyleBackColor = true;
            this.btn_Left.Click += new System.EventHandler(this.btn_Left_Click);
            // 
            // btn_Right
            // 
            this.btn_Right.Location = new System.Drawing.Point(659, 735);
            this.btn_Right.Name = "btn_Right";
            this.btn_Right.Size = new System.Drawing.Size(124, 23);
            this.btn_Right.TabIndex = 4;
            this.btn_Right.Text = "Право";
            this.btn_Right.UseVisualStyleBackColor = true;
            this.btn_Right.Click += new System.EventHandler(this.btn_Right_Click);
            // 
            // gamePanel
            // 
            this.gamePanel.BackColor = System.Drawing.Color.GreenYellow;
            this.gamePanel.Location = new System.Drawing.Point(12, 12);
            this.gamePanel.Name = "gamePanel";
            this.gamePanel.Size = new System.Drawing.Size(1203, 708);
            this.gamePanel.TabIndex = 0;
            this.gamePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.gamePanel_Paint);
            // 
            // btn_Foward
            // 
            this.btn_Foward.Location = new System.Drawing.Point(812, 730);
            this.btn_Foward.Name = "btn_Foward";
            this.btn_Foward.Size = new System.Drawing.Size(94, 31);
            this.btn_Foward.TabIndex = 5;
            this.btn_Foward.Text = "Вперёд";
            this.btn_Foward.UseVisualStyleBackColor = true;
            this.btn_Foward.Click += new System.EventHandler(this.btn_Foward_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1227, 769);
            this.Controls.Add(this.btn_Foward);
            this.Controls.Add(this.btn_Right);
            this.Controls.Add(this.btn_Left);
            this.Controls.Add(this.btn_SizeIncrease);
            this.Controls.Add(this.btn_SizeDecrese);
            this.Controls.Add(this.btn_Start);
            this.Controls.Add(this.gamePanel);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private DoubleBufferedPanel gamePanel;
        private System.Windows.Forms.Button btn_Start;
        private System.Windows.Forms.Timer globalGameTimer;
        private System.Windows.Forms.Button btn_SizeDecrese;
        private System.Windows.Forms.Button btn_SizeIncrease;
        private System.Windows.Forms.Button btn_Left;
        private System.Windows.Forms.Button btn_Right;
        private System.Windows.Forms.Button btn_Foward;
    }
}

